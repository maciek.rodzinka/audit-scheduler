const express = require('express');
const router = express.Router();
const Task = require('../models/Task');
const { isAuthenticated } = require('./middleware/authMiddleware');
const { roleCheck } = require('./middleware/roleMiddleware');

// Endpoint for task completion rates and user productivity
router.get('/task-completion', [isAuthenticated, roleCheck(['Admin', 'Manager'])], async (req, res) => {
  try {
    const taskCompletionRates = await Task.aggregate([
      {
        $group: {
          _id: '$completed',
          count: { $sum: 1 }
        }
      }
    ]);

    const userProductivity = await Task.aggregate([
      {
        $match: { completed: true }
      },
      {
        $group: {
          _id: '$assignedTo',
          completedTasks: { $sum: 1 }
        }
      },
      {
        $lookup: {
          from: 'users',
          localField: '_id',
          foreignField: '_id',
          as: 'user'
        }
      },
      {
        $unwind: '$user'
      },
      {
        $project: {
          user: '$user.username',
          completedTasks: 1
        }
      }
    ]);

    res.json({ taskCompletionRates, userProductivity });
  } catch (error) {
    console.error('Error fetching analytics data:', error);
    console.error(error.stack);
    res.status(500).json({ message: error.message });
  }
});

// Endpoint for task durations and scheduling efficiency
router.get('/task-durations', [isAuthenticated, roleCheck(['Admin', 'Manager'])], async (req, res) => {
  try {
    const taskDurations = await Task.aggregate([
      {
        $project: {
          duration: 1,
          title: 1
        }
      }
    ]);

    res.json({ taskDurations });
  } catch (error) {
    console.error('Error fetching task durations:', error);
    console.error(error.stack);
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;