const roleCheck = (roles) => (req, res, next) => {
  if (req.session && req.session.userId && roles.includes(req.session.role)) {
    next();
  } else {
    res.status(403).send('Access Denied: You do not have permission to perform this action');
  }
};

module.exports = {
  roleCheck,
};