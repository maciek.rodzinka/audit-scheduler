const express = require('express');
const router = express.Router();
const Task = require('../models/Task');
const moment = require('moment');
const { isAuthenticated } = require('./middleware/authMiddleware');
const { roleCheck } = require('./middleware/roleMiddleware');
const User = require('../models/User');
const { mergeContinuousTasks } = require('../utils/taskMerger');


router.get('/tasks/events', isAuthenticated, async (req, res) => {
  try {
      let tasks;
      console.log("gettasksevents apiroute");
  
      if (req.session.role === 'Admin') {
        tasks = await Task.find({}).populate('assignedTo', 'username calendarColor');
      } else if (req.session.role === 'Manager') {
        const manager = await User.findById(req.session.userId);
        const users = await User.find({ manager: manager._id });
        const userIds = users.map(user => user._id);
        tasks = await Task.find({ assignedTo: { $in: userIds } }).populate('assignedTo', 'username calendarColor');
      } else if (req.session.role === 'Analyst') {
        tasks = await Task.find({ assignedTo: req.session.userId }).populate('assignedTo', 'username calendarColor');
      } else if (req.session.role === 'Reader') {
        // Fetch users with role "Analyst" for Reader role
        const users2 = await User.find({ role: 'Analyst' });
        const userIds = users2.map(user => user._id.toString());
  
        // Initialize results object
        let results = {};
  
        // Run mergeContinuousTasks for each user
        for (const userId of userIds) {
          const result = await mergeContinuousTasks(userId);
          results[userId] = result;
        }
  
        // Create events for the merged tasks
        const events = Object.keys(results).flatMap(userId => {
          const user = users2.find(user => user._id.toString() === userId);
          return results[userId].mergedTasks.map(task => {
            let end = task.endDate ? moment(task.endDate) : moment(task.startDate).add(task.duration, 'hours');
  
            // Get color for the assigned user from the calendarColor field
            const color = user.calendarColor || '#007bff'; // Default color if not set
  
            return {
              id: task._id,
              title: `${task.title} (Assigned to: ${user.username})`,
              start: moment(task.startDate).toISOString(),
              end: end.toISOString(),
              duration: task.duration,
              allDay: !task.startDate || !task.endDate,
              backgroundColor: color,  // Custom background color
              borderColor: color,
              userRole: req.session.role,
              username: user.username
            };
          });
        });
  
        return res.json(events);
      } else {
        return res.status(403).json({ message: 'Access Denied' });
      }
  
      const events = tasks.map(task => {
        let end = task.endDate ? moment(task.endDate) : moment(task.startDate).add(task.duration, 'hours');
  
        // Get color for the assigned user from the calendarColor field
        const color = task.assignedTo.calendarColor || '#007bff'; // Default color if not set
  
        return {
          id: task._id,
          title: `${task.title} (Assigned to: ${task.assignedTo.username})`,
          start: moment(task.startDate).toISOString(),
          end: end.toISOString(),
          duration: task.duration,
          allDay: !task.startDate || !task.endDate,
          backgroundColor: color,  // Custom background color
          borderColor: color,
          userRole: req.session.role
        };
      });
  
      res.json(events);
    } catch (error) {
      console.error('Error fetching tasks:', error);
      res.status(500).json({ error: 'Error fetching tasks' });
  }
});
  

router.post('/tasks/:taskId/update', async (req, res) => {
    try {
        console.log("posttasksupdate apiroute");
        const { taskId } = req.params;
        const { startDate, endDate, duration } = req.body;
        const updateData = {
            startDate: startDate,
            endDate: endDate,
            duration: duration
        };
        const updatedTask = await Task.findByIdAndUpdate(taskId, updateData, { new: true });

        if (!updatedTask) {
            return res.status(404).json({ message: "Task not found" });
        }

        res.json({ message: "Task updated successfully", task: updatedTask });
    } catch (error) {
        console.error('Error updating task:', error); // Log the entire error
        console.error(error.stack);
        res.status(500).json({ message: error.message });
    }
});

// Endpoint to update working hours
router.put('/users/:userId/working-hours', [isAuthenticated, roleCheck(['Admin', 'Manager'])], async (req, res) => {
  try {
    console.log("putusersworkinghours apiroute");
    const { start, end } = req.body;
    const { userId } = req.params;
    const updatedUser = await User.findByIdAndUpdate(userId, { 'workingHours.start': start, 'workingHours.end': end }, { new: true });
    if (!updatedUser) {
      return res.status(404).json({ message: "User not found" });
    }
    res.json({ message: "Working hours updated successfully", workingHours: updatedUser.workingHours });
  } catch (error) {
    console.error('Error updating working hours:', error);
    console.error(error.stack);
    res.status(500).json({ message: error.message });
  }
});

// Endpoint to fetch working hours for a user
router.get('/users/:userId/working-hours', isAuthenticated, async (req, res) => {
  try {
    console.log("getusersworkinghours apiroute");
    const { userId } = req.params;
    const user = await User.findById(userId, 'workingHours');
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }
    res.json({ workingHours: user.workingHours });
  } catch (error) {
    console.error('Error fetching working hours:', error);
    console.error(error.stack);
    res.status(500).json({ message: error.message });
  }
});

// New API endpoint for updating a user's role
router.put('/users/:userId/role', [isAuthenticated, roleCheck(['Admin', 'Manager'])], async (req, res) => {
  try {
    console.log("putusersrole apiroute");
    console.log("whatsinparam", req.params);
    console.log("whatsinbody", req.body);
    const { userId } = req.params;
    const { role, manager, description } = req.body;

    const updatedUser = await User.findByIdAndUpdate(
      userId,
      {
        role: role,
        manager: manager ? manager : null, // Set to null if not provided
        description: description ? description : 'No description provided' // Set to default if not provided
      },
      { new: true }
    );

    if (!updatedUser) {
      return res.status(404).json({ message: "User not found" });
    }

    res.json({ message: "User role, manager, and description updated successfully", role: updatedUser.role, manager: updatedUser.manager, description: updatedUser.description });
  } catch (error) {
    console.error('Error updating user role, manager, and description:', error); // Log the entire error
    console.error(error.stack);
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;