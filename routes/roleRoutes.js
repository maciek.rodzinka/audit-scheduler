const express = require('express');
const { isAuthenticated } = require('./middleware/authMiddleware');
const { roleCheck } = require('./middleware/roleMiddleware');
const User = require('../models/User');
const { generateToken } = require('../utils/tokenGenerator');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const router = express.Router();

router.get('/role-management', roleCheck(['Admin', 'Manager']), (req, res) => {
  // Fetching user's role from the session
  const userRole = req.session.role;
  res.render('roleManagement', { userRole: userRole });
});

router.get('/working-hours', [isAuthenticated, roleCheck(['Admin', 'Manager'])], (req, res) => {
  const userId = req.session.userId; // Assuming the user ID is stored in the session
  // Fetching user's role from the session for consistency
  const userRole = req.session.role;
  res.render('workingHours', { userId: userId, userRole: userRole });
});

// Added route handler for serving the role management form
router.get('/role-form', [isAuthenticated, roleCheck(['Admin', 'Manager'])], (req, res) => {
  // Fetching user's role from the session for consistency
  const userRole = req.session.role;
  res.render('roleForm', { userRole: userRole });
});

// Add this at the end of the file
router.get('/analytics', [isAuthenticated, roleCheck(['Admin', 'Manager'])], (req, res) => {
  // Fetching user's role from the session for consistency
  const userRole = req.session.role;
  res.render('analytics', { userRole: userRole });
});

// Route for generating API tokens for Admins
router.get('/api-token-generation', [isAuthenticated, roleCheck(['Admin'])], (req, res) => {
  // Fetching user's role from the session for consistency
  const userRole = req.session.role;
  res.render('apiTokenGeneration', { userRole: userRole });
});

router.post('/api-token-generation', [isAuthenticated, roleCheck(['Admin'])], async (req, res) => {
  try {
    const token = await generateToken();
    const encodedToken = jwt.sign({ token }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1h' });
    // Ideally, here you would store the encodedToken in the database associated with the user or application
    await User.updateOne({ _id: req.session.userId }, { $set: { apiToken: encodedToken } });
    res.json({ apiToken: encodedToken });
  } catch (error) {
    console.error('Error generating API token:', error);
    console.error(error.stack); // Log the entire error stack
    res.status(500).json({ message: 'Failed to generate API token due to an internal error', error: error.message });
  }
});

module.exports = router;