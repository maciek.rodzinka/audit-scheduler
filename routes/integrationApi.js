const express = require('express');
const router = express.Router();
const Task = require('../models/Task');
const User = require('../models/User');
const tokenAuthMiddleware = require('../middleware/tokenAuthMiddleware');

// Fetch all tasks
router.get('/tasks', async (req, res) => {
  try {
    const tasks = await Task.find({});
    res.json(tasks);
  } catch (error) {
    console.error('Error fetching tasks:', error);
    console.error(error.stack); // Log the entire error stack
    res.status(500).json({ message: error.message });
  }
});

// Fetch a specific user's tasks
router.get('/users/:userId/tasks', async (req, res) => {
  try {
    console.log("getuserstasks integrationapiroute");
    const { userId } = req.params;
    const tasks = await Task.find({ assignedTo: userId });
    res.json(tasks);
  } catch (error) {
    console.error('Error fetching user tasks:', error);
    console.error(error.stack); // Log the entire error stack
    res.status(500).json({ message: error.message });
  }
});

// Create a new task with token authentication
router.post('/tasks', tokenAuthMiddleware, async (req, res) => {
  try {
    const newTask = new Task(req.body);
    const savedTask = await newTask.save();
    res.status(201).json(savedTask);
  } catch (error) {
    console.error('Error creating task:', error);
    console.error(error.stack); // Log the entire error stack
    res.status(400).json({ message: error.message });
  }
});

module.exports = router;