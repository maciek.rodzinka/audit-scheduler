const express = require('express');
const router = express.Router();
const User = require('../models/User');
const { isAuthenticated } = require('./middleware/authMiddleware');
const { roleCheck } = require('./middleware/roleMiddleware');

// GET endpoint to fetch all users
router.get('/users', [isAuthenticated, roleCheck(['Admin', 'Manager'])], async (req, res) => {
  try {
    const users = await User.find({}, 'username role'); // Fetching only username and role for privacy
    res.json(users);
  } catch (error) {
    console.error('Error fetching users:', error);
    console.error(error.stack);
    res.status(500).json({ message: error.message });
  }
});

// GET endpoint to fetch users with the 'Manager' role
router.get('/users/managers', [isAuthenticated, roleCheck(['Admin'])], async (req, res) => {
  try {
    console.log("getusersmanagers userapiroute");
    const managers = await User.find({ role: 'Manager' }, 'username'); // Fetching only username for privacy
    res.json(managers);
  } catch (error) {
    console.error('Error fetching managers:', error);
    console.error(error.stack);
    res.status(500).json({ message: error.message });
  }
});

// PUT endpoint to update user's role and manager
router.put('/users/:userId/role', [isAuthenticated, roleCheck(['Admin', 'Manager'])], async (req, res) => {
  const { userId } = req.params;
  console.log("putusersrole userapiroute");
  const { role, manager } = req.body;
  try {
    console.log("userid", userId, "manager:", manager, "role:", role);  
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
    user.role = role;
    if (manager) user.manager = manager;
    await user.save();
    res.json({ message: 'User role and manager updated successfully', role: user.role, manager: user.manager });
  } catch (error) {
    console.error('Error updating user role and assigning manager:', error);
    console.error(error.stack);
    res.status(500).json({ message: error.message });
  }
});

router.get('/user/userDetails/:username', async (req, res) => {
  try {
    const user = await User.findOne({ username: req.params.username }).populate('manager', 'username');
    console.log("user", req.params);

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    res.render('tasks/userDetails', { user });
  } catch (error) {
    console.error('Error fetching user details:', error);
    res.status(500).json({ error: 'Error fetching user details' });
  }
});

// Route to handle the description submission
router.post('/user/description', async (req, res) => {
  if (!req.session.userId) {
    return res.status(401).json({ message: 'Unauthorized' });
  }

  const { description } = req.body;
  const userId = req.session.userId;

  console.log("postusersdescription", description);
  console.log("req.session.userId", userId);

  try {
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    user.description = description || 'No description available'; // Handle null description
    await user.save();

    res.redirect(`/tasks`);
  } catch (error) {
    console.error('Error updating user description:', error);
    res.status(500).json({ error: 'Error updating user description' });
  }
});

module.exports = router;