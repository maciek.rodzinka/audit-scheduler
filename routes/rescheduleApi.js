const express = require('express');
const Task = require('../models/Task');
const { isAuthenticated } = require('./middleware/authMiddleware');
const { rescheduleTasksForUser } = require('../utils/taskScheduler');
const router = express.Router();

router.post('/reschedule/:taskId', isAuthenticated, async (req, res) => {
    try {
        const { taskId } = req.params;
        const task = await Task.findById(taskId);
        if (!task) {
            return res.status(404).send('Task not found');
        }
        await rescheduleTasksForUser(task.assignedTo);
        res.status(200).send('Task rescheduled successfully');
    } catch (error) {
        console.error(`Error rescheduling task: ${error.message}`);
        console.error(error.stack);
        res.status(500).send('Error rescheduling task');
    }
});

module.exports = router;