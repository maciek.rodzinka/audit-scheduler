const express = require('express');
const Task = require('../models/Task');
const User = require('../models/User');
const { isAuthenticated } = require('./middleware/authMiddleware');
const mongoose = require('mongoose');
const { rescheduleTasksForUser } = require('../utils/taskScheduler');
const router = express.Router();
const moment = require('moment');


// Middleware to check for admin, manager, or analyst role for task creation and update
const checkAdminOrManagerOrAnalystForSelf = async (req, res, next) => {
  const userRole = req.session.role;
  const userId = req.session.userId;

  let assignedToId;

  console.log('Request Body:', req.body);

  if (req.body.assignedTo && typeof req.body.assignedTo === 'object' && req.body.assignedTo !== null) {
    assignedToId = req.body.assignedTo._id.toString();
  } else if (typeof req.body.assignedTo === 'string') {
    assignedToId = req.body.assignedTo;
  }

  if (['Admin', 'Manager'].includes(userRole)) {
    next();
  } else if (userRole === 'Analyst') {
    if (req.method === 'POST') {
      if (assignedToId && assignedToId === userId.toString()) {
        next();
      } else {
        res.status(403).send('Access Denied');
      }
    } else if (req.method === 'GET') {
      next();
    }
  } else {
    res.status(403).send('Access Denied');
  }
};

router.get('/tasks', isAuthenticated, async (req, res) => {
  try {
    let tasks;
    if (req.session.role === 'Admin') {
      tasks = await Task.find({}).populate('assignedTo', 'username');
    } else if (req.session.role === 'Manager') {
      const manager = await User.findById(req.session.userId);
      const users = await User.find({ manager: manager._id });
      const userIds = users.map(user => user._id);
      tasks = await Task.find({ assignedTo: { $in: userIds } }).populate('assignedTo', 'username');
    } else if (req.session.role === 'Analyst') {
      tasks = await Task.find({ assignedTo: req.session.userId }).populate('assignedTo', 'username');
    } else {
      return res.status(403).send('Access Denied');
    }
    res.render('tasks/list', { tasks });
  } catch (error) {
    console.error(`Error fetching tasks: ${error.message}`);
    console.error(error.stack);
    res.status(500).send(error.message);
  }
});

router.get('/tasks/new', isAuthenticated, checkAdminOrManagerOrAnalystForSelf, async (req, res) => {
  try {
    let users = [];
    if (req.session.role === 'Admin') {
      users = await User.find({}).select('username _id');
    } else if (req.session.role === 'Manager') {
      const manager = await User.findById(req.session.userId);
      users = await User.find({ manager: manager._id }).select('username _id');
    }
    res.render('tasks/new', { users, userRole: req.session.role, userId: req.session.userId });
  } catch (error) {
    console.error(`Error fetching users: ${error.message}`);
    console.error(error.stack);
    res.status(500).send(error.message);
  }
});

// Route to display the description form
router.get('/tasks/description', (req, res) => {
  if (!req.session.userId) {
    return res.status(401).json({ message: 'Unauthorized' });
  }
  res.render('tasks/description');
});

router.post('/tasks', isAuthenticated, checkAdminOrManagerOrAnalystForSelf, async (req, res) => {
  try {
    console.log("posttasks taskroutes", req.body);
    const taskData = req.body;
    if (req.session.role === 'Analyst') {
      taskData.assignedTo = req.session.userId; // Automatically assign task to themselves
    } else if (req.session.role === 'Manager') {
      // Ensure a Manager cannot assign tasks to users outside their team
      const manager = await User.findById(req.session.userId);
      const users = await User.find({ manager: manager._id });
      const userIds = users.map(user => user._id.toString());
      if (!userIds.includes(taskData.assignedTo)) {
        return res.status(403).send('Access Denied: Cannot assign tasks to users outside your team.');
      }
    }
    if (!mongoose.Types.ObjectId.isValid(taskData.assignedTo)) {
      return res.status(400).send('Invalid user ID for assignedTo field');
    }
    const userTasks = await Task.find({ assignedTo: taskData.assignedTo });

    console.log("rescheduling for task creation.");

    const result = await rescheduleTasksForUser(taskData.assignedTo, taskData);

    if (result.status === 'error') {
      return res.status(500).send(result.message);
    }

    res.redirect('/tasks');
  } catch (error) {
    console.error(`Error creating task: ${error.message}`);
    console.error(error.stack);
    res.status(500).send(`Error creating task: ${error.message}`);
  }
});

module.exports = router;

router.get('/tasks/edit/:id', isAuthenticated, checkAdminOrManagerOrAnalystForSelf, async (req, res) => {
  try {
    const task = await Task.findById(req.params.id).populate('assignedTo');
    console.log("gettaskedit taskroutes Task found:", task);
    if (!task) {
      return res.status(404).send('Task not found');
    }
    if (req.session.role === 'Analyst' && task.assignedTo._id.toString() !== req.session.userId) {
      return res.status(403).send('Access Denied');
    }
    let users = [];
    if (['Admin', 'Manager'].includes(req.session.role)) {
      users = await User.find({}).select('username _id');
    }
    res.render('tasks/edit', { task, users, userRole: req.session.role });
  } catch (error) {
    console.error(`Error fetching task for edit: ${error.message}`);
    console.error(error.stack);
    res.status(500).send(error.message);
  }
});

router.post('/tasks/edit/:id', isAuthenticated, checkAdminOrManagerOrAnalystForSelf, async (req, res) => {
  try {
    const taskData = req.body;
    const updateFields = {};

    console.log("posttasksedit", taskData);

    if (req.session.role === 'Analyst') {
      taskData.assignedTo = req.session.userId;
    }

    // Handle the completed checkbox
    const isCompleted = taskData.completed === 'on';
    updateFields.completed = isCompleted;

    if (['Admin', 'Manager'].includes(req.session.role) && taskData.projectHandover) {
      updateFields.projectHandover = new Date(taskData.projectHandover);
    }

    if (!mongoose.Types.ObjectId.isValid(taskData.assignedTo)) {
      return res.status(400).send('Invalid user ID for assignedTo field');
    }

    // Update other fields
    updateFields.title = taskData.title;
    updateFields.description = taskData.description;
    updateFields.priority = taskData.priority;
    updateFields.tags = taskData.tags.split(',').map(tag => tag.trim());
    updateFields.duration = taskData.duration;
    updateFields.assignedTo = taskData.assignedTo;
    updateFields.notEarlierThan = taskData.notEarlierThan;
    updateFields.projectHandover = taskData.projectHandover;

    // If the task is marked as completed, set the end date to the current date
    if (isCompleted) {
      updateFields.endDate = moment().toDate();
    }

    console.log("updatefields", updateFields);
    console.log("req.params.id", req.params.id);

    const updateResult = await Task.findByIdAndUpdate(req.params.id, updateFields, { new: true });
    if (!updateResult) {
      return res.status(404).send('Task not found for update');
    }

    console.log("updateresult", updateResult);

    // If the task is marked as completed, update the user's startCalendarDate
    if (isCompleted) {
      const newStartCalendarDate = moment().add(1, 'days').toDate();
      await User.findByIdAndUpdate(taskData.assignedTo, { startCalendarDate: newStartCalendarDate });
    }

    // Reschedule tasks for the user
    await rescheduleTasksForUser(taskData.assignedTo);

    res.redirect('/tasks');
  } catch (error) {
    console.error(`Error updating task: ${error.message}`);
    console.error(error.stack);
    res.status(500).send(error.message);
  }
});

router.get('/tasks/detail/:id', isAuthenticated, async (req, res) => {
  try {
    const task = await Task.findById(req.params.id).populate('assignedTo');
    if (!task) {
      return res.status(404).send('Task not found');
    }
    res.render('tasks/detail', { task });
  } catch (error) {
    console.error(`Error fetching task details: ${error.message}`);
    console.error(error.stack);
    res.status(500).send(error.message);
  }
});

const isValidDate = (dateString) => {
  const date = new Date(dateString);
  return !isNaN(date.getTime());
};

router.get('/tasks/search', isAuthenticated, async (req, res) => {
  try {
    const { searchQuery } = req.query;
    if (!searchQuery) {
      return res.status(400).send('Search query is required.');
    }
    const regex = new RegExp(searchQuery, 'i');
    const users = await User.find({ username: regex }).select('_id');
    const userIds = users.map(user => user._id);

    let searchConditions = [
      { title: regex },
      { description: regex },
      { tags: { $in: [regex] } },
      { assignedTo: { $in: userIds } }
    ];

    if (isValidDate(searchQuery)) {
      searchConditions.push(
        { startDate: { $gte: new Date(searchQuery), $lte: new Date(searchQuery) } },
        { endDate: { $gte: new Date(searchQuery), $lte: new Date(searchQuery) } }
      );
    }

    const tasks = await Task.find({ $or: searchConditions }).populate('assignedTo', 'username');
    res.json(tasks);
  } catch (error) {
    console.error(`Error searching tasks: ${error.message}`);
    console.error(error.stack);
    res.status(500).send(error.message);
  }
});

// DELETE route to delete a task
router.delete('/tasks/delete/:taskId', isAuthenticated, async (req, res) => {
  try {
    const { taskId } = req.params;
    const task = await Task.findById(taskId);

    if (!task) {
      return res.status(404).json({ message: 'Task not found' });
    }

    await Task.findByIdAndDelete(taskId);

    res.status(200).json({ message: 'Task deleted successfully' });
  } catch (error) {
    console.error('Error deleting task:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});


module.exports = router;