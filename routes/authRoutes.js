const express = require('express');
const User = require('../models/User');
const bcrypt = require('bcrypt');
const { isAuthenticated } = require('./middleware/authMiddleware');
const { roleCheck } = require('./middleware/roleMiddleware');
const mongoose = require('mongoose');
const router = express.Router();

router.get('/auth/register', (req, res) => {
  res.render('register');
});

router.post('/auth/register', async (req, res) => {
  try {
    const { username, password } = req.body;
    const existingUser = await User.findOne({ username });
    if (existingUser) {
      // Redirect to the new user form if the username already exists with an error message
      return res.redirect('/auth/new-user?error=username+exists');
    }
    console.log(req.body);
    //const hashedPassword = await bcrypt.hash(password, 10);
    //console.log(hashedPassword);
    await User.create({ username, password: password });
    // Redirect to login after successful registration
    res.redirect('/auth/login');
  } catch (error) {
    console.error('Registration error:', error);
    res.status(500).send(error.message);
  }
});

router.get('/auth/login', (req, res) => {
  res.render('login');
});

router.post('/auth/login', async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({ username });
    if (!user) {
      console.log('User not found');
      return res.status(400).send('User not found');
    }
    console.log('Input Password:', password);
    console.log('Stored Hashed Password:', user.password);
    console.log('Password Type:', typeof password);
    console.log('Stored Password Type:', typeof user.password);


    // Check bcrypt.compare directly
    const isMatch = await bcrypt.compare(password, user.password);
    console.log('Password Match Result:', isMatch);
    
    if (isMatch) {
      req.session.userId = user._id;
      req.session.role = user.role;  // Include role in session
      return res.redirect('/');
    } else {
      console.log('Incorrect Password');
      return res.status(400).send('Password is incorrect');
    }
  } catch (error) {
    console.error('Login error:', error);
    return res.status(500).send(error.message);
  }
});

router.get('/auth/logout', (req, res) => {
  req.session.destroy(err => {
    if (err) {
      console.error('Error during session destruction:', err);
      return res.status(500).send('Error logging out');
    }
    res.redirect('/auth/login');
  });
});

// New route for serving the newUser.ejs form
router.get('/auth/new-user', [isAuthenticated, roleCheck(['Admin', 'Manager'])], (req, res) => {
  res.render('newUser');
});

// New POST route for processing the new user form submission
router.post('/auth/new-user', [isAuthenticated, roleCheck(['Admin', 'Manager'])], async (req, res) => {
  try {
    const { username, password, role, manager } = req.body;
    const existingUser = await User.findOne({ username });
    if (existingUser) {
      return res.redirect('/auth/new-user?error=username+exists');
    }
    const hashedPassword = password;
    // Validate role
    const allowedRoles = ['Analyst', 'Manager', 'Reader'];
    if (!allowedRoles.includes(role)) {
      return res.status(400).send('Invalid role selected');
    }
    // Validate manager field for applicable roles and ensure it contains a valid user ID
    if (manager && mongoose.Types.ObjectId.isValid(manager)) {
      const managerExists = await User.findById(manager);
      if (!managerExists) {
        return res.status(400).send('Invalid manager selected');
      }
      if (role !== 'Analyst') {
        return res.status(400).send('Manager can only be assigned to Analysts');
      }
    }
    await User.create({ username, password: hashedPassword, role, manager: (role === 'Analyst' && manager) ? manager : undefined });
    res.redirect('/role-management?success=user+created');
  } catch (error) {
    console.error('Error creating new user:', error);
    res.status(500).send(error.message);
  }
});

module.exports = router;