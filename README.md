```
# AuditScheduler

AuditScheduler is a web-based application designed to manage task queues for a team of analysts, aiming to streamline task management, enhance productivity, and ensure efficient scheduling of tasks within working hours. It supports various user roles with different permissions, including Admin, Manager, Analyst, and Reader, and offers a wide range of features from task management to reporting and analytics.

## Overview

The AuditScheduler app utilizes a three-tier architecture:
- **Presentation Layer:** Built with Bootstrap for a responsive design, ensuring compatibility across devices.
- **Application Logic Layer:** Implemented in Node.js with Express, handling web server operations, routing, and API endpoints.
- **Data Storage Layer:** MongoDB is used for storing user information, tasks, dependencies, and schedules.

Authentication is managed through Express sessions with password hashing for security. The system supports role-based access control (RBAC) for different user roles with varying permissions. Task scheduling logic considers user working hours and task dependencies to optimize assignments and notifications.

### Technologies Used
- Node.js
- MongoDB (with an option to use MongoDB Atlas for cloud storage)
- Express
- Mongoose
- bcrypt
- express-session
- connect-mongo
- ejs
- nodemailer
- dotenv
- Bootstrap

## Features

- **User Roles and Permissions:** Different levels of access and functionality for Admin, Manager, Analyst, and Reader roles.
- **Task Management:** Tasks come with attributes like title, description, priority, tags, duration, start and end dates, and dependencies.
- **Calendar and Scheduling:** Daily, weekly, and monthly views, considering working hours when arranging tasks.
- **Notifications and Reminders:** In-app notifications and email alerts for upcoming tasks or deadlines.
- **Integration with Other Tools:** API endpoints are provided for integration with other project management tools.
- **Reporting and Analytics:** Reporting on task completion rates and user productivity, along with analytics on task durations and scheduling efficiency.
- **Security:** Username/password authentication, password hashing, and token-based authentication for API access.

## Getting Started

### Requirements

- Node.js installed on your system.
- MongoDB installed locally or an account on MongoDB Atlas.
- An email service provider for nodemailer setup.

### Quickstart

1. Clone the repository to your local machine.
2. Navigate to the project directory and install dependencies:
   ```
   npm install
   ```
3. Copy `.env.example` to `.env` and fill in your MongoDB URL, session secret, and other environment variables as needed.
4. Start the server:
   ```
   npm start
   ```
5. Access the application through `http://localhost:3000` or the port you specified in your `.env` file.

### License

Copyright (c) 2024.

This project is proprietary and not open source.
```