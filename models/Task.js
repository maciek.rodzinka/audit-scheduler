const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  priority: { type: String, enum: ['Low', 'Medium', 'High'], required: true },
  tags: [{ type: String }],
  duration: { type: Number, required: true },
  startDate: { type: Date },
  endDate: { type: Date },
  assignedTo: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  dependencies: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Task' }],
  creationDate: { type: Date, default: Date.now },
  completed: { type: Boolean, default: false },
  projectHandover: { type: Date },
  isAfterDeadline: { type: Boolean, default: false },
  notEarlierThan: { type: Date },
});

// At the end of the Task schema definition
taskSchema.index({ title: 'text', description: 'text', tags: 'text' });

const Task = mongoose.model('Task', taskSchema);

module.exports = Task;