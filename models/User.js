const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema({
  username: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  role: {
    type: String,
    required: true,
    enum: ['Admin', 'Manager', 'Analyst', 'Reader', 'none'], // Only allow these roles
    default: 'none' // Default role
  },
  workingHours: {
    start: { type: String, default: '08:00' },
    end: { type: String, default: '16:00' }
  },
  manager: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false },
  workingDays: {
    type: [{ type: String, enum: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] }],
    default: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
  },
  startCalendarDate: { type: Date, default: Date.now },
  calendarColor: { type: String, default: '#007bff' }, // Default color for calendar
  finishCalendarDate: { type: Date, default: Date.now },
  description: { type: String, default: 'No description provided' },
});

userSchema.pre('save', function(next) {
  const user = this;
  if (!user.isModified('password')) return next();
  console.log("hash in model triggered");
  bcrypt.hash(user.password, 10, (err, hash) => {
    if (err) {
      console.error('Error hashing password:', err);
      return next(err);
    }
    user.password = hash;
    next();
  });
});

const User = mongoose.model('User', userSchema);

module.exports = User;