const Queue = require('bull');
const { rescheduleTasksForUser } = require('../utils/taskScheduler');

// Create a new Bull queue for task rescheduling requests
const taskRescheduleQueue = new Queue('taskReschedule', {
  redis: {
    host: 'INPUT_REQUIRED {Redis_host}', // User needs to set Redis host
    port: 'INPUT_REQUIRED {Redis_port}', // User needs to set Redis port
  },
});

// Define the process function for the queue
taskRescheduleQueue.process(async (job, done) => {
  try {
    const { userId } = job.data;
    await rescheduleTasksForUser(userId);
    console.log(`Rescheduled tasks for user: ${userId}`);
    done();
  } catch (error) {
    console.error(`Error rescheduling tasks for user ${job.data.userId}: ${error.message}`);
    console.error(error.stack);
    done(error);
  }
});

module.exports = taskRescheduleQueue;