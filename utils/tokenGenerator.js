const crypto = require('crypto');
const jwt = require('jsonwebtoken'); // Added for JWT functionality
require('dotenv').config(); // Ensure access to environment variables

function generateToken() {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(48, (err, buffer) => {
      if (err) {
        console.error(`Error generating token: ${err.message}`);
        console.error(err.stack); // Log the entire error stack
        reject(err);
      } else {
        // Generate a JWT instead of a simple hex string
        if (!process.env.ACCESS_TOKEN_SECRET) {
          const error = new Error('ACCESS_TOKEN_SECRET is not defined in the environment variables.');
          console.error('Error generating JWT:', error);
          console.error(error.stack); // Log the entire error stack
          reject(error);
        } else {
          const token = jwt.sign({ data: buffer.toString('hex') }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1h' });
          resolve(token);
        }
      }
    });
  });
}

module.exports = { generateToken };