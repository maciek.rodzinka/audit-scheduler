const moment = require('moment');
const Task = require('../models/Task');
const User = require('../models/User'); // Adjust the path as necessary

const isWeekend = (date) => {
  const day = moment(date).day();
  return day === 0 || day === 6; // Sunday is 0 and Saturday is 6
};

const mergeContinuousTasks = async (userId) => {
  try {
    // Fetch all tasks assigned to the user
    let tasks = await Task.find({ assignedTo: userId }).sort({ startDate: 1 });

    // Filter out completed tasks
    tasks = tasks.filter(task => !task.completed);

    if (tasks.length === 0) {
      return {
        status: 'success',
        message: 'No tasks to merge',
        mergedTasks: []
      };
    }

    let mergedTasks = [];
    let currentMergedTask = null;

    // Get the username of the user
    const user = await User.findById(userId);

    for (const task of tasks) {
      if (currentMergedTask === null) {
        currentMergedTask = {
          title: user.username,
          assignedTo: task.assignedTo,
          startDate: task.startDate,
          endDate: task.endDate,
          duration: task.duration,
        };
      } else {
        let endDate = moment(currentMergedTask.endDate);
        let startDate = moment(task.startDate);
        let diffInDays = startDate.diff(endDate, 'days');

        // Calculate the number of weekend days in between
        let weekendsInBetween = 0;
        for (let date = endDate.clone().add(1, 'days'); date.isBefore(startDate, 'day'); date.add(1, 'days')) {
          if (isWeekend(date)) {
            weekendsInBetween++;
          }
        }

        if (diffInDays - weekendsInBetween <= 1) {
          // Merge tasks
          currentMergedTask.endDate = moment.max(endDate, moment(task.endDate)).toDate();
          currentMergedTask.duration += task.duration;
        } else {
          // Save the current merged task and start a new one
          mergedTasks.push(currentMergedTask);
          currentMergedTask = {
            title: user.username,
            assignedTo: task.assignedTo,
            startDate: task.startDate,
            endDate: task.endDate,
            duration: task.duration,
          };
        }
      }
    }

    // Push the last merged task if there is one
    if (currentMergedTask !== null) {
      mergedTasks.push(currentMergedTask);
    }

    return {
      status: 'success',
      message: 'Tasks merged successfully',
      mergedTasks: mergedTasks
    };
  } catch (error) {
    return {
      status: 'error',
      message: `Error merging tasks for user ${userId}: ${error.message}`
    };
  }
};

module.exports = { mergeContinuousTasks };
