const moment = require('moment');
const Task = require('../models/Task');
const User = require('../models/User'); // Adjust the path as necessary

const isWorkingDay = (date, workingDays) => {
  const dayName = date.format('dddd'); // Get the day name, e.g., 'Monday'
  return workingDays.includes(dayName); // Check if it is in the user's working days
};

const findNextWorkingDay = (date, workingDays) => {
  date.add(1, 'days'); // Move to the next day initially
  while (!isWorkingDay(date, workingDays)) {
    date.add(1, 'days');
  }
  return date;
};

const calculateScores = (tasks, currentDate) => {
  tasks.forEach(task => {
    const daysUntilDeadline = moment(task.projectHandover).diff(currentDate, 'days');
    const deadlinePressure = 1 / (daysUntilDeadline || 1); // Avoid division by zero
    task.score = deadlinePressure;
  });
};

function setToWorkingHours(date, workingHoursStart) {
  return date.clone().set({
    hour: workingHoursStart.hour(),
    minute: workingHoursStart.minute(),
    second: 0,
    millisecond: 0
  });
}

const rescheduleTasksForUser = async (userId, newTaskData) => {
  try {
    // Fetch the user's working hours and working days
    const user = await User.findById(userId);
    if (!user) {
      throw new Error(`Użytkownik o podanym ID: ${userId} nie istnieje`);
    }

    console.log("rescheduleTasksForUser", userId);

    const WORKING_HOURS_START = moment(user.workingHours.start, 'HH:mm');
    const WORKING_HOURS_END = moment(user.workingHours.end, 'HH:mm');
    const workingDays = user.workingDays;
    const startCalendarDate = setToWorkingHours(moment(user.startCalendarDate).startOf('hour'), WORKING_HOURS_START); // Adjusted to start of working hours

    // Fetch all tasks assigned to the user being updated
    let tasks = await Task.find({ assignedTo: userId });

    // Filter out completed tasks
    tasks = tasks.filter(task => !task.completed);

    if (newTaskData) {
      tasks.push(newTaskData);
    }

    // Separate tasks with and without notEarlierThan defined
    let tasksWithoutNotEarlierThan = tasks.filter(task => !task.notEarlierThan);
    let tasksWithNotEarlierThan = tasks.filter(task => task.notEarlierThan);

    let success = false;
    let overDeadlineTask = null; // To store the task that goes over the deadline

    calculateScores(tasksWithoutNotEarlierThan, startCalendarDate);

    // Sort tasks without notEarlierThan by score
    tasksWithoutNotEarlierThan.sort((a, b) => b.score - a.score);

    let currentDate = startCalendarDate.clone();
    let finalEndDate = null;

    success = true;

    while (tasksWithoutNotEarlierThan.length > 0) {
      const task = tasksWithoutNotEarlierThan.shift();

      // Ensure currentDate is within working hours
      if (currentDate.isAfter(moment(currentDate).hour(WORKING_HOURS_END.hour()).minute(WORKING_HOURS_END.minute()))) {
        currentDate = findNextWorkingDay(setToWorkingHours(currentDate.clone(), WORKING_HOURS_START), workingDays);
      }

      let startDate = currentDate.clone();
      if (!isWorkingDay(startDate, workingDays)) {
        startDate = findNextWorkingDay(startDate, workingDays);
        startDate = setToWorkingHours(startDate, WORKING_HOURS_START);
      }

      let endDate = startDate.clone();
      let durationLeft = task.duration;

      // Adjust endDate to fit within working hours
      while (durationLeft > 0) {
        if (!isWorkingDay(endDate, workingDays)) {
          endDate = findNextWorkingDay(endDate.clone().startOf('day'), workingDays);
          endDate = setToWorkingHours(endDate, WORKING_HOURS_START);
        } else {
          let workingDayEnd = endDate.clone().hour(WORKING_HOURS_END.hour()).minute(WORKING_HOURS_END.minute());
          let hoursLeftInDay = workingDayEnd.diff(endDate, 'hours', true);
          if (durationLeft > hoursLeftInDay) {
            endDate = endDate.add(hoursLeftInDay, 'hours');
            durationLeft -= hoursLeftInDay;
            endDate = findNextWorkingDay(endDate.clone().startOf('day'), workingDays);
            endDate = setToWorkingHours(endDate, WORKING_HOURS_START);
          } else {
            endDate = endDate.add(durationLeft, 'hours');
            durationLeft = 0;
          }
        }
      }

      // Check if the end date goes beyond the project handover date
      if (endDate.isAfter(moment(task.projectHandover))) {
        success = false;
        overDeadlineTask = task; // Store the task that goes over the deadline
        break;
      }

      // Update the task's start and end dates
      task.startDate = startDate.toDate();
      task.endDate = endDate.toDate();

      // Prepare the start date for the next task
      currentDate = endDate.clone();
      finalEndDate = endDate.clone(); // Track the final end date

      // Check tasksWithNotEarlierThan and move eligible ones to tasksWithoutNotEarlierThan
      tasksWithNotEarlierThan = tasksWithNotEarlierThan.filter(task => {
        if (moment(task.notEarlierThan).isSameOrBefore(finalEndDate)) {
          tasksWithoutNotEarlierThan.push(task);
          calculateScores([task], startCalendarDate); // Recalculate score for the task
          return false; // Remove this task from tasksWithNotEarlierThan
        }
        return true; // Keep the task in tasksWithNotEarlierThan
      });

      // Re-sort tasksWithoutNotEarlierThan by score
      tasksWithoutNotEarlierThan.sort((a, b) => b.score - a.score);
    }

    // Handle leftover tasks with notEarlierThan
    if (tasksWithNotEarlierThan.length > 0) {
      tasksWithNotEarlierThan.sort((a, b) => {
        if (moment(a.notEarlierThan).isSame(moment(b.notEarlierThan))) {
          const daysUntilDeadlineA = moment(a.projectHandover).diff(startCalendarDate, 'days');
          const daysUntilDeadlineB = moment(b.projectHandover).diff(startCalendarDate, 'days');
          return daysUntilDeadlineA - daysUntilDeadlineB;
        } else {
          return moment(a.notEarlierThan).diff(moment(b.notEarlierThan));
        }
      });

      for (const task of tasksWithNotEarlierThan) {
        let startDate = moment(task.notEarlierThan);

        if (finalEndDate && startDate.isBefore(finalEndDate)) {
          startDate = finalEndDate.clone();
        }

        if (!isWorkingDay(startDate, workingDays)) {
          startDate = findNextWorkingDay(startDate, workingDays);
          startDate = setToWorkingHours(startDate, WORKING_HOURS_START);
        }

        if (isWorkingDay(startDate, workingDays)) {
          startDate = setToWorkingHours(startDate, WORKING_HOURS_START);
        }

        console.log(startDate.format('YYYY-MM-DD HH:mm:ss'));

        let endDate = startDate.clone();
        let durationLeft = task.duration;

        // Adjust endDate to fit within working hours
        while (durationLeft > 0) {
          if (!isWorkingDay(endDate, workingDays)) {
            endDate = findNextWorkingDay(endDate.clone().startOf('day'), workingDays);
            endDate = setToWorkingHours(endDate, WORKING_HOURS_START);
          } else {
            let workingDayEnd = endDate.clone().hour(WORKING_HOURS_END.hour()).minute(WORKING_HOURS_END.minute());
            let hoursLeftInDay = workingDayEnd.diff(endDate, 'hours', true);
            if (durationLeft > hoursLeftInDay) {
              endDate = endDate.add(hoursLeftInDay, 'hours');
              durationLeft -= hoursLeftInDay;
              endDate = findNextWorkingDay(endDate.clone().startOf('day'), workingDays);
              endDate = setToWorkingHours(endDate, WORKING_HOURS_START);
            } else {
              endDate = endDate.add(durationLeft, 'hours');
              durationLeft = 0;
            }
          }
        }

        // Check if the end date goes beyond the project handover date
        if (endDate.isAfter(moment(task.projectHandover))) {
          success = false;
          overDeadlineTask = task; // Store the task that goes over the deadline
          break;
        }

        // Update the task's start and end dates
        task.startDate = startDate.toDate();
        task.endDate = endDate.toDate();

        // Prepare the start date for the next task
        finalEndDate = endDate.clone(); // Track the final end date
      }
    }

    if (!success) {
      return {
        status: 'error',
        message: `Nie można zaplanować zadań w terminach przekazania projektu. Zadanie "${overDeadlineTask.title}" (ID: ${overDeadlineTask._id}) przekracza termin.`
      };
    }

    // Update the user's finishCalendarDate with the final end date
    if (finalEndDate) {
      user.finishCalendarDate = finalEndDate.toDate();
      await user.save();
    }

    // If new task data was provided, create the new task in the database
    if (newTaskData) {
      const createdTask = await Task.create(newTaskData);
      tasks.push(createdTask);
    }

    // Update all tasks in the database
    for (const task of tasks) {
      await Task.findByIdAndUpdate(task._id, { startDate: task.startDate, endDate: task.endDate });
    }

    return {
      status: 'success',
      message: 'Zadania zostały pomyślnie przełożone',
    };
  } catch (error) {
    return {
      status: 'error',
      message: `Błąd podczas planowania zadań dla użytkownika ${userId}: ${error.message}`
    };
  }
};

module.exports = { rescheduleTasksForUser };
