const { describe, it, expect } = require('@jest/globals');
const { rescheduleTasksForUser } = require('../utils/taskScheduler');
const Task = require('../models/Task');
const mongoose = require('mongoose');
const moment = require('moment');

describe('Task Scheduler', () => {
  beforeAll(async () => {
    await mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true });
  });

  afterAll(async () => {
    await mongoose.disconnect();
  });

  describe('rescheduleTasksForUser', () => {
    it('should reschedule all tasks for a user without overlap and within working hours', async () => {
      // Setup: Create a user and assign multiple tasks
      const userId = new mongoose.Types.ObjectId();
      const tasks = [
        {
          _id: new mongoose.Types.ObjectId(),
          title: 'Task 1',
          description: 'First Task',
          priority: 'High',
          duration: 2,
          startDate: moment().hour(9).toDate(),
          endDate: moment().hour(11).toDate(),
          assignedTo: userId
        },
        {
          _id: new mongoose.Types.ObjectId(),
          title: 'Task 2',
          description: 'Second Task',
          priority: 'Medium',
          duration: 3,
          startDate: moment().add(1, 'days').hour(9).toDate(),
          endDate: moment().add(1, 'days').hour(12).toDate(),
          assignedTo: userId
        }
      ];

      // Mock Task.find and Task.findByIdAndUpdate to simulate database operations
      Task.find = jest.fn().mockResolvedValue(tasks);
      Task.findByIdAndUpdate = jest.fn().mockImplementation((id, update) => Promise.resolve({ _id: id, ...update }));

      await rescheduleTasksForUser(userId.toString());

      // Verify that tasks are rescheduled correctly
      expect(Task.find).toHaveBeenCalledWith({ assignedTo: userId });
      expect(Task.findByIdAndUpdate.mock.calls.length).toBe(tasks.length);

      // Check that tasks are rescheduled without overlap and within working hours
      const updatedTasks = Task.findByIdAndUpdate.mock.calls.map(call => call[1]);
      expect(moment(updatedTasks[0].startDate).hour()).toBeGreaterThanOrEqual(9);
      expect(moment(updatedTasks[0].endDate).hour()).toBeLessThanOrEqual(17);
      expect(moment(updatedTasks[1].startDate).isAfter(updatedTasks[0].endDate)).toBeTruthy();
    });
  });
});