const jwt = require('jsonwebtoken');
require('dotenv').config();

const tokenAuthMiddleware = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null) return res.sendStatus(401); // No token found

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      console.error('Token verification error:', err);
      console.error(err.stack); // Log the entire error stack
      return res.sendStatus(403); // Invalid token
    }
    req.user = user;
    next();
  });
};

module.exports = tokenAuthMiddleware;