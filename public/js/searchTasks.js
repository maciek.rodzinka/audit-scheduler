document.addEventListener('DOMContentLoaded', function() {
  const searchForm = document.querySelector('form');
  if (searchForm && searchForm.elements['searchQuery']) {
    searchForm.addEventListener('submit', function(e) {
      e.preventDefault();
      const searchQuery = this.elements['searchQuery'].value;
      fetch(`/tasks/search?searchQuery=${encodeURIComponent(searchQuery)}`)
        .then(response => response.json())
        .then(tasks => {
          const taskListContainer = document.querySelector('.list-group');
          if (taskListContainer) {
            taskListContainer.innerHTML = ''; // Clear current list
            tasks.forEach(task => {
              const taskElement = document.createElement('a');
              taskElement.href = `/tasks/edit/${task._id}`;
              taskElement.classList.add('list-group-item', 'list-group-item-action');
              taskElement.innerHTML = `
                <h5 class="mb-1">${task.title}</h5>
                <p class="mb-1">Priority: ${task.priority}</p>
                <small>Due: ${task.endDate ? new Date(task.endDate).toDateString() : 'No deadline'}</small>
              `;
              taskListContainer.appendChild(taskElement);
            });
          }
        })
        .catch(error => {
          console.error('Error searching tasks:', error.message);
          console.error(error.stack);
        });
    });
  }
});