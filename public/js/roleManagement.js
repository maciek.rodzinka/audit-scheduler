document.addEventListener('DOMContentLoaded', function() {
    const roleForm = document.getElementById('roleForm');
    const roleSelect = document.getElementById('roleSelect');
    const userIdInput = document.getElementById('userId');
    const feedbackContainer = document.getElementById('feedback');

    roleForm.addEventListener('submit', async function(e) {
        e.preventDefault();
        const userId = userIdInput.value;
        const selectedRole = roleSelect.value;
        console.log("roleManagement");
        try {
            const response = await fetch('/api/users/' + userId + '/role', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ role: selectedRole }),
            });

            if (response.ok) {
                const data = await response.json();
                feedbackContainer.textContent = `Role updated successfully to ${data.role} for user ID: ${userId}`;
                feedbackContainer.classList.add('alert-success');
            } else {
                throw new Error('Failed to update role');
            }
        } catch (error) {
            console.error('Error updating role:', error);
            feedbackContainer.textContent = error.message;
            feedbackContainer.classList.add('alert-danger');
        }
    });
});