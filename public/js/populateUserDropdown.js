document.addEventListener('DOMContentLoaded', function() {
    fetch('/api/users')
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to fetch users');
        }
        return response.json();
    })
    .then(users => {
        const userSelect = document.getElementById('userSelect');
        users.forEach(user => {
            const option = document.createElement('option');
            option.value = user._id;
            option.textContent = user.username;
            userSelect.appendChild(option);
        });

        const managerSelect = document.getElementById('managerSelect');
        // Add a blank option for manager
        const blankOption = document.createElement('option');
        blankOption.value = '';
        blankOption.textContent = 'None';
        blankOption.selected = true; // Ensure 'None' is selected by default
        managerSelect.appendChild(blankOption);

        // Populate the manager dropdown
        users.filter(user => user.role === 'Manager').forEach(manager => {
            const option = document.createElement('option');
            option.value = manager._id;
            option.textContent = manager.username;
            managerSelect.appendChild(option);
        });
    })
    .catch((error) => {
        console.error('Error populating user dropdown:', error);
        console.error(error.stack);
    });
});