document.addEventListener('DOMContentLoaded', function() {
    const roleForm = document.getElementById('roleForm');
    const userSelect = document.getElementById('userSelect');
    const roleSelect = document.getElementById('roleSelect');
    const managerSelect = document.getElementById('managerSelect');
    const descriptionInput = document.getElementById('description');
    const feedbackContainer = document.getElementById('feedback');

    roleForm.addEventListener('submit', async function(e) {
        e.preventDefault();
        const userId = userSelect.value;
        const selectedRole = roleSelect.value;
        const selectedManager = managerSelect.value;
        const description = descriptionInput.value;
        console.log("updateRole");

        const requestBody = { role: selectedRole };

        // Only add manager to the request body if it is not empty
        if (selectedManager) {
            requestBody.manager = selectedManager;
        }

        // Only add description to the request body if it is not empty
        if (description) {
            requestBody.description = description;
        }

        try {
            const response = await fetch('/api/users/' + userId + '/role', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(requestBody),
            });

            if (response.ok) {
                const data = await response.json();
                feedbackContainer.textContent = `Rola pomyślnie zaktualizowana do ${data.role}` +
                    (data.manager ? ` i przypisano menadżera: ${data.manager}` : '') +
                    (data.description ? ` z opisem: ${data.description}` : '') +
                    ` for user ID: ${userId}`;
                feedbackContainer.classList.remove('alert-danger');
                feedbackContainer.classList.add('alert-success');
                feedbackContainer.style.display = 'block';
            } else {
                throw new Error('Nie udało się zaktualizować roli, przypisać menedżera ani dodać opisu');
            }
        } catch (error) {
            console.error('Błąd podczas aktualizowania roli, przypisywania menedżera lub dodawania opisu:', error);
            feedbackContainer.textContent = error.message;
            feedbackContainer.classList.remove('alert-success');
            feedbackContainer.classList.add('alert-danger');
            feedbackContainer.style.display = 'block';
        }
    });
});