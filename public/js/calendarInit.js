document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var userRole = '<%= session.role %>'; // Ensure this renders correctly
    var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        editable: false, // Disable dragging and dropping
        eventResizableFromStart: false, // Disable resizing from the start
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,dayGridDay'
        },
        locale: 'pl', // Ustawienie języka na polski
        buttonText: {
            today:    'Dzisiaj',
            month:    'Miesiąc',
            week:     'Tydzień',
            day:      'Dzień',
            list:     'Plan'
        },
        events: function(fetchInfo, successCallback, failureCallback) {
            fetch(`/api/tasks/events?role=${userRole}`)
            .then(response => response.json())
            .then(tasks => {
                const events = tasks.map(task => {
                    return {
                        id: task.id,
                        title: task.title,
                        start: task.start,
                        end: task.end,
                        allDay: task.allDay,
                        backgroundColor: task.backgroundColor,
                        borderColor: task.borderColor,
                        extendedProps: {
                            userRole: task.userRole,
                            username: task.username
                        }
                    };
                });
                successCallback(events);
            })
            .catch(error => {
                console.error('Error fetching tasks:', error);
                console.error(error.stack);
                failureCallback(error);
            });
        },
        eventClick: function(info) {
            if (info.event.extendedProps.userRole != 'Reader') {
                const taskId = info.event.id;
                window.location.href = `/tasks/detail/${taskId}`;
            } else {
                const taskId = info.event.extendedProps.username;
                window.location.href = `/api/user/userDetails/${taskId}`;
            }
        }
    });
    calendar.render();
});