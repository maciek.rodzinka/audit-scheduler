document.addEventListener('DOMContentLoaded', function() {
  fetch('/api/task-completion')
  .then(response => response.json())
  .then(data => {
    const analyticsContent = document.getElementById('analyticsContent');
    let taskCompletionHTML = '<h3>Wskaźniki ukończenia zadań</h3><ul>';
    data.taskCompletionRates.forEach(rate => {
      taskCompletionHTML += `<li>${rate._id ? 'Ukończone' : 'Nie ukończone'}: ${rate.count}</li>`;
    });
    taskCompletionHTML += '</ul>';

    let userProductivityHTML = '<h3>Produktywność użytkownika</h3><ul>';
    data.userProductivity.forEach(user => {
      userProductivityHTML += `<li>${user.user}: ${user.completedTasks} zadań ukończonych</li>`;
    });
    userProductivityHTML += '</ul>';

    analyticsContent.innerHTML = taskCompletionHTML + userProductivityHTML;
  })
  .catch(error => {
    console.error('Error loading task completion analytics:', error);
    console.error(error.stack);
  });

  fetch('/api/task-durations')
  .then(response => response.json())
  .then(data => {
    const analyticsContent = document.getElementById('analyticsContent');
    let taskDurationsHTML = '<h3>Czas trwania zadania</h3><ul>';
    data.taskDurations.forEach(task => {
      taskDurationsHTML += `<li>${task.title}: ${task.duration} godzin</li>`;
    });
    taskDurationsHTML += '</ul>';

    analyticsContent.innerHTML += taskDurationsHTML;
  })
  .catch(error => {
    console.error('Error loading task durations:', error);
    console.error(error.stack);
  });
});