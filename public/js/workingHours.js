document.addEventListener('DOMContentLoaded', function() {
  const form = document.getElementById('workingHoursForm');
  form.addEventListener('submit', function(e) {
    e.preventDefault();
    const userId = document.getElementById('userId').value; // Dynamic user ID from hidden input
    const start = document.getElementById('start').value;
    const end = document.getElementById('end').value;
    console.log("workingHours");
    fetch(`/api/users/${userId}/working-hours`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ start, end }),
    })
    .then(response => response.json())
    .then(data => {
      alert('Godziny pracy zostały pomyślnie zaktualizowane');
      console.log('Godziny pracy zaktualizowane:', data);
    })
    .catch((error) => {
      console.error('Błąd podczas aktualizowania godzin pracy:', error);
      console.error(error.stack);
    });
  });
});